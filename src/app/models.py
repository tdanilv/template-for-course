from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=255)
    handle = models.CharField(max_length=255)
    td_id = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.name}"


class Meta:
    verbose_name = "Person"
    verbose_name_plural = "People"
